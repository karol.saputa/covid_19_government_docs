# COVID-19 pandemic - Government Websites documents
## Text from PDF documents on .gov websites in 2020


* annotated_on - date of document annotation in System
* country
* info_date - date of document creation
* keywords - keywords from PDF document and COVID-19 keywords from text
* language
* organization
* original_text - extracted text
* pdf_path - name of the PDF docuement
* scrap_date - date of document gathering
* section - subject of section of document
* title - first words of documents, translated to English
* web_page - URL of document
* domain
* status - System label, `subject_accepted` means it is processed and qualified as on the subject of COVID-19 pandemic


